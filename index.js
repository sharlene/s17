/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function userDetails(){
	let userName = prompt("What is your Name?");
	let userAge = prompt("How old are you?");
	let userAddress = prompt("Where do you live?");

	console.log("Hello, " + userName);
	console.log("You are " + userAge + " years old");
	console.log("You live in " + userAddress); 
};

userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function faveMusicalArtists() {
	let faveArtist1 = ("Linkin Park");
	let faveArtist2 = ("Em Beihold");
	let faveArtist3 = ("Anson Seabra");
	let faveArtist4 = ("Gracia Abrams");
	let faveArtist5 = ("Sasha Alex Sloan");

	console.log("1. "+ faveArtist1);
	console.log("2. "+ faveArtist2);
	console.log("3. "+ faveArtist3);
	console.log("4. "+ faveArtist4);
	console.log("5. "+ faveArtist5);
};

faveMusicalArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function faveMovies() {
	let faveMovie1 = ("A Serbian Film");
	let faveMovie2 = ("Spirited Away");
	let faveMovie3 = ("Ready Player One");
	let faveMovie4 = ("10 Cloverfield Lane");
	let faveMovie5 = ("Perfume: The Story of a Murderer");

	console.log("1. " + faveMovie1);
	console.log("Rotten Tomatoes Rating is: 47%");
	console.log("2. " + faveMovie2);
	console.log("Rotten Tomatoes Rating is: 97%");
	console.log("3. " + faveMovie3);
	console.log("Rotten Tomatoes Rating is: 72%");
	console.log("4. " + faveMovie4);
	console.log("Rotten Tomatoes Rating is: 90%");
	console.log("5. " + faveMovie5);
	console.log("Rotten Tomatoes Rating is: 59%");

};

faveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



function printUserFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	alert("Thank you for sharing the names of your friends.");
	
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3);
};

printUserFriends();